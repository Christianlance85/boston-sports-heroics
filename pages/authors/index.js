// pages/posts/[slug].js

import { getAuthor, getAllAuthors } from "../../lib/posts";
import { Box, Heading, Container, Center, Stack } from "@chakra-ui/react";
import Head from "next/head";

// PostPage page component
export default function AuthorPage(props) {
  // console.log(props);
  // Render post title and content in the page from props
  return (
    <Box m="5em">
      {/* <Head>
        <title>{props.post.meta_title}</title>
        <meta name="description" content={props.post.meta_description} />
      </Head>
      <Center maxW="100%">
        <Stack>
          <Heading>{props.post.title}</Heading>
          <Box dangerouslySetInnerHTML={{ __html: props.post.html }} />
        </Stack>
      </Center> */}
    </Box>
  );
}


export async function getStaticProps(context) {
    const author = await getAllAuthors();
    

  if (!author) {
    return {
      notFound: true,
    };
  }

  return {
    props: { author },
  };
}
