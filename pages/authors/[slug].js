import { getAuthor, getAllAuthors } from "../../lib/posts";
import { Box, Heading, Container, Center, Stack } from "@chakra-ui/react";
import Head from "next/head";



const AuthorPage = (props) => {
  // console.log(props)
  return (
    <Box m="5em">
      <Head>
        <title>{props.author.meta_title}</title>
        <meta name="description" content={props.author.meta_description} />
      </Head>
      <Center maxW="100%">
        <Stack>
          {/* <Image src=''/> */}
          <Heading>{props.author.name}</Heading>
          <Box dangerouslySetInnerHTML={{ __html: props.author.bio }} />
        </Stack>
      </Center>
    </Box>
  );
};

export async function getStaticPaths() {
  const authors = await getAllAuthors();
  const paths = authors.map((author) => ({
    params: { slug: author.slug },
  }));

  return { paths, fallback: false };
}

export async function getStaticProps(context) {
  const author = await getAuthor(context.params.slug);

  if (!author) {
    return {
      notFound: true,
    };
  }

  return {
    props: { author },
  };
}
export default AuthorPage;
