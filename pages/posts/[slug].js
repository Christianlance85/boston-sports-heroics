// pages/posts/[slug].js

import { getSinglePost, getPosts } from "../../lib/posts";
import { Box, Heading, Container, Center, Stack } from "@chakra-ui/react";
import Head from "next/head";

// PostPage page component
export default function PostPage(props) {
  // Render post title and content in the page from props
  return (
    <Box m="5em">
      <Head>
        <title>{props.post.meta_title}</title>
        <meta name="description" content={props.post.meta_description} />
      </Head>
      <Center maxW="100%">
        <Stack>
          <Heading>{props.post.title}</Heading>
          <Box dangerouslySetInnerHTML={{ __html: props.post.html }} />
        </Stack>
      </Center>
    </Box>
  );
}

export async function getStaticPaths() {
  const posts = await getPosts();

  // Get the paths we want to create based on posts
  const paths = posts.map((post) => ({
    params: { slug: post.slug },
  }));

  // { fallback: false } means posts not found should 404.
  return { paths, fallback: false };
}

// Pass the page slug over to the "getSinglePost" function
// In turn passing it to the posts.read() to query the Ghost Content API
export async function getStaticProps(context) {
  const post = await getSinglePost(context.params.slug);

  if (!post) {
    return {
      notFound: true,
    };
  }

  return {
    props: { post },
    revalidate: 60,
  };
}
