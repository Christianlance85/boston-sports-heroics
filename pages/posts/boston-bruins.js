import { getBBruins } from "../../lib/posts";
import Link from "next/link";
import Image from "next/image";
import {
  Box,
  Text,
  Heading,
  Grid,
  GridItem,
  Center,
  Stack,
} from "@chakra-ui/react";

import dayjs from "dayjs";

export async function getStaticProps(context) {
  const posts = await getBBruins();

  if (!posts) {
    return {
      notFound: true,
    };
  }

  return {
    revalidate: 60,
    props: { posts },
  };
}

export default function (props) {
  return (
    <Box backgroundColor="menuGray">
      <Box backgroundColor="black" p="5em">
        <Stack>
          <Heading textAlign="center" color="white" fontSize="xl">
            - Boston Bruins -
          </Heading>
        </Stack>
      </Box>
      <ul>
        <Grid
          templateColumns={{
            base: null,
            md: "repeat(2,1fr)",
            lg: "repeat(3,1fr)",
          }}
          gap={1}
          ml={{ base: "3em", xxl: "15em" }}
          mr={{ base: "3em", xxl: "15em" }}
          templateRows="repeat(2, 1fr)"
        >
          {props.posts
            .map((post) => (
              <GridItem
                colSpan={{ base: 1, lg: 1 }}
                rowSpan={2}
                minW={"17em"}
                maxW={"25em"}
                w={"100%"}
                align="left"
                borderColor="white"
                foregroundColor="white"
                mb="3em"
                _hover={{
                  transform: "scale(1.1)",
                  cursor: "pointer",
                }}
                marginInline="auto"
              >
                <Link href={`/posts/${post.slug}`}>
                  <li key={post.id} style={{ "list-style": "none" }}>
                    <Box
                      border={{ base: "none", md: "1px" }}
                      borderBottom="1px"
                      borderRadius={{ base: "none", md: "0.5rem" }}
                      m="1em"
                      p="3em"
                    >
                      {post.feature_image ? (
                        <Center>
                          <Image
                            bgPos="center"
                            objectFit="contain"
                            height="200rem"
                            width="200rem"
                            src={post.feature_image}
                          />
                        </Center>
                      ) : (
                        ""
                      )}
                      <Heading>{post.title}</Heading>
                      <Text color={"smokeygray"} fontSize="15px">
                        {dayjs(post.updated_at).format("MMMM, D, YYYY")}
                      </Text>
                      <Text color="grey">{`${post.excerpt.slice(
                        0,
                        150
                      )}...`}</Text>
                    </Box>
                  </li>
                </Link>
              </GridItem>
            ))
            .slice(0, 10)}
        </Grid>
      </ul>
    </Box>
  );
}
