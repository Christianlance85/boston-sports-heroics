import { ChakraProvider } from "@chakra-ui/react";
import theme from '../theme/theme'
import NavBar from '../components/NavBar'



function MyApp({ Component, pageProps }) {
  window.dataLayer = window.dataLayer || [];
  function gtag() {
    dataLayer.push(arguments);
  }
  gtag("js", new Date());

  gtag("config", "G-1SQ0W7076Z");
  return (
    <ChakraProvider resetCSS theme={theme}>
      
      <NavBar />
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default MyApp;
