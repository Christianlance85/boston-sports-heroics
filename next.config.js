module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["res-3.cloudinary.com", "static.ghost.org", "res.cloudinary.com"],
  },
};
