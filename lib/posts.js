import GhostContentAPI from "@tryghost/content-api";

const api = new GhostContentAPI({
  url: "https://oth-product-backend.herokuapp.com",
  key: "a6e9e1b3eeaf33aaa71dc81464",
  version: "v3",
});

export async function getPosts() {
  return await api.posts
    .browse({
      include: ["tags", "authors"],
      limit: "all",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getFeatured() {
  return await api.posts
    .browse({
      filter: "tag:Featured",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getBCollege() {
  return await api.posts
    .browse({
      filter: "tag:Boston-College",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getBBruins() {
  return await api.posts
    .browse({
      filter: "tag:Boston-Bruins",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getBCeltics() {
  return await api.posts
    .browse({
      filter: "tag:Boston-Celtics",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getBRedsox() {
  return await api.posts
    .browse({
      filter: "tag:Boston-Redsox",
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getNewEnglandPatriots() {
  return await api.posts
    .browse({
      filter: "tag:New-England-Patriots",
    })
    .catch((err) => {
      console.error(err);
    });
}

export async function getSinglePost(postSlug) {
  return await api.posts
    .read({
      slug: postSlug,
    })
    .catch((err) => {
      console.error(err);
    });
}

export async function getAuthor(authorSlug) {
  return await api.authors
    .read({
      slug: authorSlug,
    })
    .catch((err) => {
      console.error(err);
    });
}
export async function getAllAuthors() {
  return await api.authors.browse({
    include: 'authors'
  }).catch((err) => {
    console.error(err);
  });
}
