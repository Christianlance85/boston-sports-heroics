import {
  createClient,
  dedupExchange,
  cacheExchange,
  ssrExchange,
} from '@urql/core'
import { devtoolsExchange } from '@urql/devtools'
import storage from '../utils/storage'
import { multipartFetchExchange } from '@urql/exchange-multipart-fetch'

const isServerSide = typeof window === 'undefined'

// The `ssrExchange` must be initialized with `isClient` and `initialState`
const ssrCache = ssrExchange({
  isClient: !isServerSide,
  initialState: !isServerSide ? window.__URQL_DATA__ : undefined,
})

const client = createClient({
  url: isServerSide
    ? `${process.env.NEXT_PUBLIC_GHOST}`
    : `${process.env.NEXT_PUBLIC_GHOST}`,
  fetchOptions: () => {
    const token = storage.getToken();
    return {
      headers: {
        authorization: token ? `Bearer ${token}` : "",
      },
    };
  },
  exchanges: [
    dedupExchange,
    cacheExchange,
    ssrCache, // Add `ssr` in front of the `fetchExchange`
    devtoolsExchange,
    multipartFetchExchange,
  ],
});

declare global {
  interface Window {
    __URQL_DATA__?: undefined
  }
}

export { client, ssrCache }
