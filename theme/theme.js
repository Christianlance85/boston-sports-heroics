import { extendTheme, ThemeConfig } from "@chakra-ui/react";
import { createBreakpoints } from "@chakra-ui/theme-tools";

const config = {
  initialColorMode: "light",
  useSystemColorMode: false,
};

// These are the same fonts that we have on the home page.
const fonts = {
  heading: `'Ovo', serif`,
  body: `'Poppins', sans-serif`,
};

const sizes = {
  max: "72rem",
};
// breakpoints are for have responsive points based on screen size rather than manually setting media queries.
const breakpoints = createBreakpoints({
  sm: "30em",
  md: "48em",
  lg: "62em",
  xl: "80em",
  xxl: "96em",
});

const theme = extendTheme({
  ...config,
  colors: {
    // This is our color scheme on the PHP Platform.
    primary: "#091F2F",
    secondary: "#800000",
    commonGray: "#6f6f6f",
    smokeyGray: "rgba(62, 69, 94, 0.05)",
    ghostWhite: "#ffffff80",
    menuGrey: "#c3c3c326",
    snowWhite: "#FFFFFF0",
    newBlue: "#288BE4",
    bcGold: "#b29d6c",
    celticGreen: "#007A33",
    redsoxRed: "#C60C30",
    patriotBlue: '#002244',
    patriotRed: '#C60C30'
  },
  fonts,
  breakpoints,
  sizes,
});

export default theme;
