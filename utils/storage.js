const storagePrefix = "oth-product_";

const storage = {
  getToken: () => {
    try {
      if (process.browser) {
        return JSON.parse(
          window.localStorage.getItem(`${storagePrefix}token`)
        );
      }
    } catch (error) {
      if (error) storage.clearToken();
    }
  },
  setToken: (token) => {
    if (process.browser) {
      window.localStorage.setItem(
        `${storagePrefix}token`,
        JSON.stringify(token)
      );
    }
  },
  clearToken: () => {
    if (process.browser) {
      window.localStorage.removeItem(`${storagePrefix}token`);
    }
  },
};

export default storage;
